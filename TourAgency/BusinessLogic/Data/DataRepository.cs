﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class DataRepository<T> : IData<T> where T : BaseEntity
    {
        private string fileName;
        private List<T> list;
        private ISerialization<List<T>> sl = new Serialization<List<T>>();

        public DataRepository(string fileName)
        {
            this.fileName = fileName;
            ReadFromDB(fileName);
        }

        public List<T> GetList()
        {
            return list;
        }

        public void Add(T item)
        {
            list.Add(item);
        }

        public void Update(T item)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Equals(item))
                {
                    list[i] = item;
                    return;
                }
            }
        }

        public void Remove(T item)
        {
            list.Remove(item);
        }

        public void SaveToDB()
        {
            sl.FileInXml(fileName, list);
        }

        public void ReadFromDB(string filename)
        {
            list = sl.FileOutXml(filename);
        }
    }
}
