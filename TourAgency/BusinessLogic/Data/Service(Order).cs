﻿using System.Collections.Generic;

namespace BusinessLogic
{
    public partial class Service
    {
        public List<Order> GetOrders()
        {
            return orderRepo.GetList();
        }

        public bool AddNewOrder(Order item)
        {
            orderRepo.Add(item);
            DoEvent(Entity.Order);
            return true;
        }

        public void UpdateOrder(Order item)
        {
            orderRepo.Update(item);
            DoEvent(Entity.Order);
        }

        public void RemoveOrder(Order item)
        {
            orderRepo.Remove(item);
            DoEvent(Entity.Order);
        }
    }
}
