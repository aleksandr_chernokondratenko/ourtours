﻿using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic
{
    public partial class Service
    {
        public List<Tour> GetTours()
        {
            return tourRepo.GetList();
        }

        public List<Tour> GetClientSByTour(Client tour)
        {
            return orderRepo.GetList().
                        Where(s => s.ClientOfAgency.Equals(tour)).
                        Select(s => s.TourOfAgency).ToList<Tour>();
        }

        public bool AddNewTour(Tour item)
        {
            tourRepo.Add(item);
            DoEvent(Entity.Tour);
            return true;
        }

        public void UpdateTour(Tour item)
        {
            tourRepo.Update(item);
            DoEvent(Entity.Tour);
        }

        public void RemoveTour(Tour item)
        {
            tourRepo.Remove(item);
            DoEvent(Entity.Tour);
        }
    }
}
