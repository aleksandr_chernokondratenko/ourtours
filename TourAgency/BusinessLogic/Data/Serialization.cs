﻿using System.IO;
using System.Xml.Serialization;

namespace BusinessLogic
{

    public class Serialization<T> : ISerialization<T>
    {
        private XmlSerializer xSerializer = new XmlSerializer(typeof(T));
        public Serialization() { }

        public void FileInXml(string NameFile, T data)      // сериализация - write
        {
            if (File.Exists(NameFile)) File.Delete(NameFile);
            using (var fileIn = new FileStream(NameFile, FileMode.Create))
            {
                xSerializer.Serialize(fileIn, data);
            }
        }

        public T FileOutXml(string NameFile)      // десериализация - read
        {
           // if (!File.Exists(NameFile)) throw new FileNotFoundException("File not found!", NameFile);
            using (var fileOut = new FileStream(NameFile, FileMode.Open))
            {
                return (T)xSerializer.Deserialize(fileOut);
            }
        }
    }
}
