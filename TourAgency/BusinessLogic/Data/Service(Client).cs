﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public partial class Service
    {
        public List<Client> GetClients()
        {
            return clientRepo.GetList();
        }

        public List<Client> GetClientSByTour(Tour tour)
        {
            return orderRepo.GetList().
                        Where(s => s.TourOfAgency.Equals(tour)).
                        Select(s=>s.ClientOfAgency).ToList<Client>();
        }

        public bool AddNewClient(Client item)
        {
            clientRepo.Add(item);
            DoEvent(Entity.Client);
            return true;
        }

        public void UpdateClient(Client item)
        {
            clientRepo.Update(item);
            DoEvent(Entity.Client);
        }

        public void RemoveClient(Client item)
        {
            clientRepo.Remove(item);
            DoEvent(Entity.Client);
        }
    }
}
