﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{

    public class AgencyEventArgs : EventArgs
    {
        public string Message { get; set; }
        
        public Entity NameEntity { get; set; }

        public AgencyEventArgs(string mes, Entity nameEntity)
        {
            Message = mes;
            NameEntity = nameEntity;
        }
    }
}
