﻿using System;
using System.Collections.Generic;

namespace BusinessLogic
{

    public partial class Service
    {
        #region Event
        public event EventHandler<AgencyEventArgs> EventShowEntities;

        public void DoEvent(Entity nameEntity)
        {
            OnRaiseEvent(new AgencyEventArgs("Какое-то событие", nameEntity));
        }
        protected virtual void OnRaiseEvent(AgencyEventArgs e)
        {
            EventHandler<AgencyEventArgs> handler = EventShowEntities;
            if (handler != null)
            {
                //e.Message += String.Format(" произошло в {0} ", DateTime.Now.ToString());
                handler(this, e);
            }
        } 
        #endregion

        #region Поля
        private static DataRepository<Client> clientRepo;
        private static DataRepository<Tour> tourRepo;
        private static DataRepository<Order> orderRepo;
        #endregion

        #region Конструктор
        /// <summary>
        /// Конструктор сервиса
        /// </summary>
        /// <param name="fileNames"> список файлов, содержащий путь к файлу для каждого набора данных</param>
        public Service(List<String> fileNames)
        {
            clientRepo = new DataRepository<Client>(fileNames[0]);
            tourRepo = new DataRepository<Tour>(fileNames[1]);
            orderRepo = new DataRepository<Order>(fileNames[2]);
        } 
        #endregion

        /// <summary>
        /// Сохраняет данные по всем коллекциям БД
        /// </summary>
        public void SaveData()
        {
            clientRepo.SaveToDB();
            tourRepo.SaveToDB();
            orderRepo.SaveToDB();
        }

    }
}
