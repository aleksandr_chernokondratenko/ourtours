﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
        interface ISerialization<T>
        {
            void FileInXml(string NameFile, T data);
            T FileOutXml(string NameFile);
        }
    
}
