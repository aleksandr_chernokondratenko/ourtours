﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public interface IData<T> 
    {
        void Add(T obj);
        void Remove(T obj);
        void Update(T obj);
        void SaveToDB();
        void ReadFromDB(string filename);

    }
}
