﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [Serializable]
    public class Order : BaseEntity
    {
        public Client ClientOfAgency { get; set; }
        public Tour TourOfAgency { get; set; }
        public Period PeriodOfTour { get; set; }
        public double Cost { get; set; }
        public int QuantityOfPersons { get; set; }

        public Order() { }

        public Order(int id, Client clientOfAgency, Tour tourOfAgency, Period periodOfTour,
            int quantityOfPersons)
        {
            Id = id;
            ClientOfAgency = clientOfAgency;
            TourOfAgency = tourOfAgency;
            Cost = tourOfAgency.Price * quantityOfPersons * (1 + clientOfAgency.Discount / 100);
            QuantityOfPersons = quantityOfPersons;
            PeriodOfTour = periodOfTour;
        }

        public override string ToString()
        {
            return string.Format("{0,-18}{1,-10} - {2,-10}  {3,2}   {4:0,##} $  {5}",
                ClientOfAgency.Fio,
                PeriodOfTour.StartDate.ToShortDateString(),
                PeriodOfTour.EndDate.ToShortDateString(),
                QuantityOfPersons, 
                Cost,
                TourOfAgency.Name);
        }
    }
}
