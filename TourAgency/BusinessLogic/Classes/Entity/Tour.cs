﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [Serializable]
    public class Tour : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Hotel { get; set; }
        public double Price { get; set; }

        public Tour() { }

        public Tour(int id, string name, string description, 
                        string country, string city, 
                        string hotel, double price)
        {
            this.Id = id;
            this.Name = name;
            this.Description = description;
            this.Country = country;
            this.City = city;
            this.Hotel = hotel;
            this.Price = price;
        }

        public override string ToString()
        {
            return string.Format("{0,-12}{1,-10}{2,-10}{3,-10}{4,-8}{5}", Name, Country, City, Hotel, Price, Description);
        }
    }
}
