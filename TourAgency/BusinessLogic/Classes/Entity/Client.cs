﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    [Serializable]
    public class Client : BaseEntity
    {
        public string Fio { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public double Discount { get; set; }

        public Client() { }

        public Client(int id, string fio, string tel, string email, double discount)
        {
            Id = id;
            Fio = fio;
            Tel = tel;
            Email = email;
            Discount = discount;
        }

        public override string ToString()
        {
            return string.Format("{0,-20}{1,-20}{2,-30}{3:0.#}", Fio, Tel, Email, Discount);
        }
    }
}
