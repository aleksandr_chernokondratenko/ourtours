﻿#region Using
using System;
using System.Collections.Generic;
#endregion

#region BusinessLogic InitEntities
namespace BusinessLogic
{
    public class InitEntities
    {
        #region Populate data 
        /// <summary>
        /// Инициализирует набор данных по клиентам в файл БД
        /// </summary>
        /// <returns>набор данных IList</returns>
        public static List<Client> InitClients()
        {
            List<Client> listOfClients = new List<Client>();
            listOfClients.Add(new Client(1, "Johnny Depp", "380675625888", "johnny.depp@gmail.com", 20));
            listOfClients.Add(new Client(2, "Leonardo Dicaprio", "380638989026", "leonardo.dicaprio@gmail.com", 25));
            listOfClients.Add(new Client(3, "Jason Statham", "380956522545", "jason.statham@gmail.com", 15));
            listOfClients.Add(new Client(4, "Vin Diesel", "380975622584", "vin.diesel@gmail.com", 10));
            listOfClients.Add(new Client(5, "Tom Cruise", "3805056317778", "tom.cruise@gmail.com", 20));
            return listOfClients;
        }

        public static List<Tour> InitTours()
        {
            List<Tour> listOfTours = new List<Tour>();
            listOfTours.Add(new Tour(1, "Tour to Rome(Italy)", "Awesome tour to Rome", "Italy", "Rome", "Hotel1", 12000));
            listOfTours.Add(new Tour(2, "Tour to London", "Awesome tour to London(Great Britain)", "Great Britain", "London", "Hotel2", 14000));
            listOfTours.Add(new Tour(3, "Tour to Copenhagen", "Awesome tour to CopenhagenDenmark)", "Denmark", "Copenhagen", "Hotel3", 13000));
            listOfTours.Add(new Tour(4, "Tour to Los Angeles", "Awesome tour to LAs(USA)", "USA", "Los Angeles", "Hotel4", 20000));
            listOfTours.Add(new Tour(5, "Tour to Paris", "Awesome tour to Paris(France)", "France", "Paris", "Hotel5", 14000));
            return listOfTours;
        }

        public static List<Order> InitOrders()
        {
            List<Order> listOfOrders = new List<Order>();
            Tour tour1 = new Tour(1, "Tour to Rome(Italy)", "Awesome tour to Rome", "Italy", "Rome", "Hotel1", 12000);
            Tour tour2 = new Tour(2, "Tour to London", "Awesome tour to London(Great Britain)", "Great Britain", "London", "Hotel2", 14000);
            Tour tour3 = new Tour(3, "Tour to Copenhagen", "Awesome tour to CopenhagenDenmark)", "Denmark", "Copenhagen", "Hotel3", 13000);
            Tour tour4 = new Tour(4, "Tour to Los Angeles", "Awesome tour to LA(USA)", "USA", "Los Angeles", "Hotel4", 20000);
            Tour tour5 = new Tour(5, "Tour to Paris", "Awesome tour to Paris(France)", "France", "Paris", "Hotel5", 14000);
            Client client1 = new Client(1, "Johnny Depp", "380675625888", "johnny.depp@gmail.com", 20);
            Client client2 = new Client(2, "Leonardo Dicaprio", "380638989026", "leonardo.dicaprio@gmail.com", 25);
            Client client3 = new Client(3, "Jason Statham", "380956522545", "jason.statham@gmail.com", 15);
            Client client4 = new Client(4, "Vin Diesel", "380975622584", "vin.diesel@gmail.com", 10);
            Client client5 = new Client(5, "Tom Cruise", "3805056317778", "tom.cruise@gmail.com", 20);
            DateTime dateTime1 = new DateTime(2015, 11, 20);
            DateTime dateTime2 = new DateTime(2015, 11, 23);
            DateTime dateTime3 = new DateTime(2015, 11, 26);
            DateTime dateTime4 = new DateTime(2015, 11, 28);
            DateTime dateTime5 = new DateTime(2015, 12, 01);
            DateTime dateTime6 = new DateTime(2015, 12, 05);
            DateTime dateTime7 = new DateTime(2015, 12, 10);
            DateTime dateTime8 = new DateTime(2015, 12, 14);
            DateTime dateTime9 = new DateTime(2015, 12, 18);
            DateTime dateTime10 = new DateTime(2015, 12, 26);
            Period period1 = new Period(dateTime1, dateTime3);
            Period period2 = new Period(dateTime2, dateTime4);
            Period period3 = new Period(dateTime3, dateTime5);
            Period period4 = new Period(dateTime3, dateTime4);
            Period period5 = new Period(dateTime4, dateTime6);
            Period period6 = new Period(dateTime7, dateTime9);
            Period period7 = new Period(dateTime6, dateTime8);
            Period period8 = new Period(dateTime5, dateTime8);
            Period period9 = new Period(dateTime5, dateTime4);
            Period period10 = new Period(dateTime7, dateTime10);

            listOfOrders.Add(new Order(1, client1, tour1, period1, 1));
            listOfOrders.Add(new Order(2, client2, tour2, period2, 1));
            listOfOrders.Add(new Order(3, client3, tour1, period1, 3));
            listOfOrders.Add(new Order(4, client4, tour4, period2, 1));
            listOfOrders.Add(new Order(5, client5, tour4, period2, 2));
            listOfOrders.Add(new Order(6, client5, tour3, period6, 1));
            listOfOrders.Add(new Order(7, client1, tour3, period5, 2));

            return listOfOrders;
        }
        #endregion
    }
} 
#endregion
