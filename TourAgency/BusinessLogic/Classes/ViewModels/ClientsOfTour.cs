﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class ClientsOfTour
    {
        public Tour TourOfClients { get; set; }
        public List<Client> Clients { get; set; }
        public ClientsOfTour(Tour tourOfClients, List<Client> clients)
        {
            TourOfClients = tourOfClients;
            Clients = clients;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.Append(TourOfClients);
            sb.AppendLine(Environment.NewLine);
            for (int i = 0; i < Clients.Count; i++)
            {
                sb.Append(i+1 + ". ");
                sb.AppendLine(Clients[i].ToString());
            }
            return sb.ToString();
        }
    }
}
