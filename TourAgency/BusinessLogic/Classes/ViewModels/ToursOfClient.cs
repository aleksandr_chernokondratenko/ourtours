﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogic
{
    public class ToursOfClient
    {
        public Client ClientOfTour { get; set; }
        public List<Tour> Tours { get; set; }
        public ToursOfClient(Client clientOfTour, List<Tour> tours)
        {
            this.ClientOfTour = clientOfTour;
            this.Tours = tours;
        }
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendLine();
            sb.Append(ClientOfTour);
            sb.AppendLine(Environment.NewLine);
            for (int i = 0; i < Tours.Count; i++)
            {
                sb.Append(i + 1 + ". ");
                sb.AppendLine(Tours[i].ToString());
            }
            return sb.ToString();
        }
    }
}
