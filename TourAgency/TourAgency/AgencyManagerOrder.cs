﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace TourAgency
{
    partial class AgencyManager
    {
        private void WorkOfJournal()
        {
            dictMenuAction = new Dictionary<int, CallMethod>()
            {
                { 0, WorkAdmin },
                { 1, AddNewOrder },
                { 2, EditOrder },
                { 3, RemoveOrder },
                { 4, ShowAllOrders },
                { 5, ShowInfoByOrder },
                { 6, MenuSortAndFilter}
            };

            while (SelectAction(OutputData.MenuJournal, OutputData.MenuInfoOrder) != 0){ }
        }

        private void MenuSortAndFilter()
        {
            dictMenuSortAndFilter = new Dictionary<int, CallMethod>()
            {
                { 0, WorkOfJournal },
                { 1, SortOrdersById },
                { 2, SortOrdersByPrice },
                { 3, SortOrderByName},
                { 4, FilterOrdersByPeriod },
                { 5, FilterOrdersByCost },
            };
            while (SelectSortAndFilter() != 0) { }
        }
        private int SelectSortAndFilter()
        {
            int choice = 0;
            bool isNum = true;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine(OutputData.MenuSortOrdersById);
            Console.WriteLine(OutputData.MenuSortOrdersByPrice);
            Console.WriteLine(OutputData.MenuSortOrdersByClientName);
            Console.WriteLine(OutputData.FilterByPeriod);
            Console.WriteLine(OutputData.FilterByCost);
            Console.WriteLine(OutputData.MenuExit);
            do
            {
                Console.Write(OutputData.Choise);
                isNum = Int32.TryParse(Console.ReadLine(), out choice);
            }
            while (!(isNum && dictMenuSortAndFilter.ContainsKey(choice)));

            dictMenuSortAndFilter[choice]();

            return choice;
        }
        private void FilterOrdersByPeriod()
        {
            Console.Write(OutputData.FilterOrdersByPeriod);
            DateTime date = Convert.ToDateTime(Console.ReadLine());

            var orders = srv.GetOrders()
                .Where(s => s.PeriodOfTour.StartDate <= date && s.PeriodOfTour.EndDate >= date).ToList();

            Console.ForegroundColor = ConsoleColor.Green;
            if (orders.Count > 0)
            {
                 Console.WriteLine(OutputData.FooterJournal);
                for (var i = 0; i < orders.Count; i++)
                {
                    Console.WriteLine("{0}. {1}", i + 1, orders[i]);
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(OutputData.MessageDelay);
            }
            else
            {
               Console.WriteLine(OutputData.FilterOrdersByPeriod_Not,date.ToShortDateString());
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
        }

        private void FilterOrdersByCost()
        {
            Console.Write(OutputData.FilterOrdersByCost);
            Double cost = Convert.ToDouble(Console.ReadLine());
            var orders = srv.GetOrders().Where(x => x.Cost.Equals(cost)).ToList();
            Console.ForegroundColor = ConsoleColor.Green;
            if (orders.Count > 0)
            {
                Console.WriteLine(OutputData.FooterJournal);
                for (int i = 0; i < orders.Count; i++)
                {
                    Console.WriteLine("{0}. {1}", i + 1, orders[i]);
                }
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(OutputData.MessageDelay);
            }
            else
            {
                Console.WriteLine(OutputData.FilterOrdersByCost_Not, cost);
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadKey();
        }

        private void SortOrdersById()
        {
            var orders = srv.GetOrders()
                .OrderBy(n => n.Id);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(OutputData.FooterJournal);
            foreach (Order order in orders)
            {
                Console.WriteLine(order);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void SortOrdersByPrice()
        {
            var orders = srv.GetOrders()
                .OrderBy(n => n.Cost);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(OutputData.FooterJournal);
            foreach (var order in orders)
            {
                Console.WriteLine(order);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void SortOrderByName()
        {
            var orders = srv.GetOrders()
                .OrderBy(n => n.ClientOfAgency.Fio);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(OutputData.FooterJournal);
            foreach (var order in orders)
            {
                Console.WriteLine(order);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

        private void ShowInfoByOrder()  // полная информайия о заказе
        {
            Console.WriteLine();
            bool isNum = true;
            int numberOfOrder = 0;
            int countOrders = srv.GetOrders().Count;
            do
            {
                Console.Write(OutputData.NumberOfOrder);
                isNum = Int32.TryParse(Console.ReadLine(), out numberOfOrder);
            }
            while (!(isNum && numberOfOrder <= countOrders));

            Console.ForegroundColor = ConsoleColor.Green;
            Order infoOrders = srv.GetOrders()[numberOfOrder - 1];
            PrintTextOnCenter(OutputData.MenuInfoOrder.Substring(3, OutputData.MenuInfoOrder.Length - 3));
            Console.WriteLine(OutputData.AddClientName + infoOrders.ClientOfAgency.Fio);
            Console.WriteLine(OutputData.AddClientTel + infoOrders.ClientOfAgency.Tel);
            Console.WriteLine(OutputData.AddClientEmail + infoOrders.ClientOfAgency.Email);
            Console.WriteLine(OutputData.AddClientDiscount + infoOrders.ClientOfAgency.Discount);
            Console.WriteLine();
            Console.WriteLine(OutputData.AddTourName + infoOrders.TourOfAgency.Name);
            Console.WriteLine(OutputData.AddTourDesc + infoOrders.TourOfAgency.Description);
            Console.WriteLine(OutputData.AddTourCountry + infoOrders.TourOfAgency.Country);
            Console.WriteLine(OutputData.AddTourCity + infoOrders.TourOfAgency.City);
            Console.WriteLine(OutputData.AddTourHotel + infoOrders.TourOfAgency.Hotel);
            Console.WriteLine(OutputData.AddTourPrice + infoOrders.TourOfAgency.Price);
            Console.WriteLine();
            Console.WriteLine(OutputData.StartTravel + infoOrders.PeriodOfTour.StartDate.ToShortDateString());
            Console.WriteLine(OutputData.EndTravel + infoOrders.PeriodOfTour.EndDate.ToShortDateString());
            Console.WriteLine(OutputData.CountPerson + infoOrders.QuantityOfPersons);
            Console.WriteLine(OutputData.Cost + infoOrders.Cost);
            Console.ForegroundColor = ConsoleColor.White;
            Console.ReadLine();
        }

        private void EditOrder()
        {
            Console.WriteLine();
            int index;
            do { Console.Write(OutputData.NumberForEdit); }
            while (!(Int32.TryParse(Console.ReadLine(), out index) && index <= srv.GetOrders().Count));
            var tmpOrder = srv.GetOrders()[index - 1];
            if (tmpOrder == null)
                return;
            else
            {
                Console.WriteLine();
                Console.WriteLine(OutputData.EditDataOrder + "\n");
                String[] newOrder = new string[13];
                newOrder[0] = InputDataForEdit(OutputData.AddClientName, tmpOrder.ClientOfAgency.Fio);
                newOrder[1] = InputDataForEdit(OutputData.AddClientTel, tmpOrder.ClientOfAgency.Tel);
                newOrder[2] = InputDataForEdit(OutputData.AddClientEmail, tmpOrder.ClientOfAgency.Email);
                newOrder[3] = InputDataForEdit(OutputData.AddClientDiscount, tmpOrder.ClientOfAgency.Discount.ToString());
                Console.WriteLine();
                newOrder[4] = InputDataForEdit(OutputData.AddTourName, tmpOrder.TourOfAgency.Name);
                newOrder[5] = InputDataForEdit(OutputData.AddTourDesc, tmpOrder.TourOfAgency.Description);
                newOrder[6] = InputDataForEdit(OutputData.AddTourCountry, tmpOrder.TourOfAgency.Country);
                newOrder[7] = InputDataForEdit(OutputData.AddTourCity, tmpOrder.TourOfAgency.City);
                newOrder[8] = InputDataForEdit(OutputData.AddTourHotel, tmpOrder.TourOfAgency.Hotel);
                newOrder[9] = InputDataForEdit(OutputData.AddTourPrice, tmpOrder.TourOfAgency.Price.ToString());
                Console.WriteLine();
                newOrder[10] = InputDataForEdit(OutputData.StartTravel, tmpOrder.PeriodOfTour.StartDate.ToShortDateString());
                newOrder[11] = InputDataForEdit(OutputData.EndTravel, tmpOrder.PeriodOfTour.EndDate.ToShortDateString());
                newOrder[12] = InputDataForEdit(OutputData.CountPerson, tmpOrder.QuantityOfPersons.ToString());

                Client newClient = new Client(tmpOrder.ClientOfAgency.Id, newOrder[0], newOrder[1], newOrder[2], double.Parse(newOrder[3].Replace(".", ",")));
                Tour newTour = new Tour(tmpOrder.TourOfAgency.Id, newOrder[4], newOrder[5], newOrder[6], newOrder[7], newOrder[8], double.Parse(newOrder[9].Replace(".", ",")));
                Period newPeriod = new Period(Convert.ToDateTime(newOrder[10]), Convert.ToDateTime(newOrder[11]));

                srv.UpdateOrder(new Order(tmpOrder.Id, newClient, newTour, newPeriod, Int32.Parse(newOrder[12])));
            }
        }

        private void RemoveOrder()
        {
            Console.WriteLine();
            bool isNum = true;
            int numberForRemove = 0;
            int countOrders = srv.GetOrders().Count;
            do
            {
                Console.Write(OutputData.NumberForRemove);
                isNum = Int32.TryParse(Console.ReadLine(), out numberForRemove);
            }
            while (!(isNum && numberForRemove <= countOrders));

            srv.RemoveClient(srv.GetClients()[numberForRemove - 1]);
        }

        private void ShowAllOrders() 
        {
            Console.Title = OutputData.MenuJournal;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Clear();
            Console.WriteLine(OutputData.FooterJournal);
            var orders = srv.GetOrders();
            for (int i = 0; i < orders.Count; i++)
            {
                Console.WriteLine("{0}. {1}", i + 1, orders[i]);
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(OutputData.MessageDelay);
            Console.ReadKey();
        }

    }
}
