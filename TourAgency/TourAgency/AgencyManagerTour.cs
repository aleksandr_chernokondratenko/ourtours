﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace TourAgency
{
    partial class AgencyManager
    {
        private void WorkOfTours()
        {
            dictMenuAction = new Dictionary<int, CallMethod>()
            {
                { 0, WorkAdmin },
                { 1, AddTour },
                { 2, EditTour },
                { 3, RemoveTour },
                { 4, ShowAllTours },
                { 5, ShowInfoByClients },
                { 6, SortByPrice}
            };

            while (SelectAction(OutputData.MenuTour, OutputData.MenuInfoTour) != 0) { }
        }

        private void ShowInfoByClients()
        {
            int index;
            do { Console.Write(OutputData.EnterNumberOfTour); }
            while (!Int32.TryParse(Console.ReadLine(), out index));
            var tmptour = srv.GetTours()[index - 1];
            var list = srv.GetClientSByTour(tmptour);
            ClientsOfTour clientsOfTour = new ClientsOfTour(tmptour,list);
            Console.WriteLine(clientsOfTour);
            Console.ReadKey();
        }

        private void AddTour()
        {
            Console.WriteLine();
            Console.WriteLine(OutputData.EnterDataTour + "\n");
            Console.Write(OutputData.AddTourName);
            string name = Console.ReadLine();
            Console.Write(OutputData.AddTourCountry);
            string country = Console.ReadLine();
            Console.Write(OutputData.AddTourCity);
            string city = Console.ReadLine();
            Console.Write(OutputData.AddTourHotel);
            string hotel = Console.ReadLine();
            double price;
            do { Console.Write(OutputData.AddTourPrice); }
            while (!double.TryParse(Console.ReadLine().Replace(".",","), out price));
            Console.Write(OutputData.AddTourDesc);
            string description = Console.ReadLine();
            // find max id 
            int id = srv.GetTours().Max(obj => obj.Id) + 1;
            srv.AddNewTour(new Tour(id, name, description, country, city, hotel, price));
        }

        private void EditTour()
        {
            Console.WriteLine();
            int index;
            do { Console.Write(OutputData.NumberForEdit); }
            while (!Int32.TryParse(Console.ReadLine(), out index));
            var tmptour = srv.GetTours()[index - 1];
            if (tmptour == null)
                return;
            else
            {
                Console.WriteLine();
                Console.WriteLine(OutputData.EditDataTour);
                String[] newTour = new string[6];
                newTour[0] = InputDataForEdit(OutputData.AddTourName, tmptour.Name);
                newTour[1] = InputDataForEdit(OutputData.AddTourDesc, tmptour.Description);
                newTour[2] = InputDataForEdit(OutputData.AddTourCountry, tmptour.Country);
                newTour[3] = InputDataForEdit(OutputData.AddTourCity, tmptour.City);
                newTour[4] = InputDataForEdit(OutputData.AddTourHotel, tmptour.Hotel);
                newTour[5] = InputDataForEdit(OutputData.AddTourPrice, tmptour.Price.ToString());

                srv.UpdateTour(new Tour(tmptour.Id, newTour[0], newTour[1], newTour[2], newTour[3], newTour[4], double.Parse(newTour[5].Replace(".", ","))));
            }
        }

        private void RemoveTour()
        {
            Console.WriteLine();
            bool isNum = true;
            int numberForRemove = 0;
            int CountTours = srv.GetTours().Count;
            do
            {
                Console.Write(OutputData.NumberForRemove);
                isNum = Int32.TryParse(Console.ReadLine(), out numberForRemove);
            }
            while (!(isNum && numberForRemove <= CountTours));

            srv.RemoveTour(srv.GetTours()[numberForRemove - 1]);
        }

        private void ShowAllTours()
        {
            //foreach (var item in srv.GetTours())
            //    Console.WriteLine(item);
            Console.Title = OutputData.MenuTour;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            PrintTextOnCenter(OutputData.MenuTour.Substring(3, OutputData.MenuTour.Length - 3));
            Console.WriteLine("\n"+OutputData.FooterTours+"\n");
            var tours = srv.GetTours();
            for (int i = 0; i < tours.Count; i++)
            {
                Console.WriteLine("{0}. {1}", i+1, tours[i]);
            }
            Console.ForegroundColor = ConsoleColor.White;
            //Console.Write(OutputData.MessageDelay);
            //Console.ReadKey();
        }

        private void SortByPrice()
        {
            var tours = srv.GetTours()
                .OrderBy(n => n.Price);
            foreach (Tour tour in tours)
            {
                Console.WriteLine(tour);
            }
            Console.ForegroundColor = ConsoleColor.White;
        }

    }
}
