﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace TourAgency
{
    partial class AgencyManager
    {
        private void WorkUser()
        {
            Console.Clear();
            Console.Title = OutputData.TitleUser;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n" + OutputData.NameAgency + "\n");

            while (true)
            {
                ShowAllTours();
                Console.WriteLine();
                AddNewOrder();
            }
        }

        private void AddNewOrder()
        {
            Client newClient;
            Period newPeriod;
            Order newOrder;

            bool isNum = true;
            int number = 0;
            int countTour = srv.GetTours().Count;
            do
            {
                Console.Write(OutputData.NumberOfTour);
                isNum = Int32.TryParse(Console.ReadLine(), out number);
            }
            while (!(isNum && number <= countTour));

            Tour newTour = srv.GetTours()[number - 1];

            Console.WriteLine("\n" + OutputData.EnterDataClient + "\n");
            Console.Write(OutputData.AddClientName);
            string fio = Console.ReadLine();
            newClient = CreateClient(fio);

            Console.Write(OutputData.StartDate);
            DateTime startDate = Convert.ToDateTime(Console.ReadLine());
            Console.Write(OutputData.EndDate);
            DateTime endDate = Convert.ToDateTime(Console.ReadLine());
            newPeriod = new Period(startDate, endDate);

            Console.Write(OutputData.QuantityPersons);
            int persons = Convert.ToInt32(Console.ReadLine());

            int idOrder = srv.GetOrders().Max(obj => obj.Id) + 1;   // find max id  

            newOrder = new Order(idOrder, newClient, newTour, newPeriod, persons);

            srv.EventShowEntities -= ShowEntities;
            srv.AddNewOrder(newOrder);
            srv.EventShowEntities += ShowEntities;

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n" + OutputData.OrderCompleted);
            Console.ForegroundColor = ConsoleColor.White;

            Console.Write(OutputData.MessageDelay);
            Console.ReadKey();
        }

        private Client CreateClient(string fio)
        {
            //return srv.GetClients().Find(obj => obj.Fio == fio);
            var tmpList = srv.GetClients();
            Client newClient = null;
            bool pr = false;  // временно 
            foreach (var item in tmpList)
            {
                if (item.Fio.Equals(fio))
                   {
                    newClient = item;
                    pr = true;
                   }
            }
           
            // if (newClient == null)
            if (!pr)
            { 
                Console.Write(OutputData.AddClientTel);
                string tel = Console.ReadLine();
                Console.Write(OutputData.AddClientEmail);
                string email = Console.ReadLine();
                double discount;
                do { Console.Write(OutputData.AddClientDiscount); }
                while (!double.TryParse(Console.ReadLine().Replace(".", ","), out discount));
                int id = srv.GetClients().Max(obj => obj.Id) + 1;   // find max id  
                newClient = new Client(id, fio, tel, email, discount);
                srv.EventShowEntities -= ShowEntities;
                srv.AddNewClient(newClient);
                srv.EventShowEntities += ShowEntities;
            }

            return newClient;
        }
    }
}
