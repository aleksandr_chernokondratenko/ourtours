﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLogic;

namespace TourAgency
{
    partial class AgencyManager
    {    
        private void WorkOfClients()
        {
            dictMenuAction = new Dictionary<int, CallMethod>()
            {
                { 0, WorkAdmin },
                { 1, AddClient },
                { 2, EditClient },
                { 3, RemoveClient },
                { 4, ShowAllClients },
                { 5, ShowInfoByTours }
            };

            while (SelectAction(OutputData.MenuClient, OutputData.MenuInfoClient) != 0) { }
        }

        private void ShowInfoByTours()
        {
            int index;
            do { Console.Write(OutputData.EnterNumberOfClient); }
            while (!Int32.TryParse(Console.ReadLine(), out index));
            var tmpclient = srv.GetClients()[index - 1];
            var list = srv.GetClientSByTour(tmpclient);
            ToursOfClient clientsOfTour = new ToursOfClient(tmpclient, list);
            Console.WriteLine(  );
            Console.WriteLine(clientsOfTour);
            Console.ReadKey();
        }

        private void AddClient()
        {
            Console.WriteLine();
            Console.WriteLine(OutputData.EnterDataClient + "\n");
            Console.Write(OutputData.AddClientName);
            string fio = Console.ReadLine();
            Console.Write(OutputData.AddClientTel);
            string tel = Console.ReadLine();
            Console.Write(OutputData.AddClientEmail);
            string email = Console.ReadLine();
            double discount;
            do { Console.Write(OutputData.AddClientDiscount); }
            while (!double.TryParse(Console.ReadLine().Replace(".", ","), out discount));
            int id = srv.GetClients().Max(obj => obj.Id) + 1;   // find max id  
            srv.AddNewClient(new Client(id, fio, tel, email, discount));
        }

        private void EditClient()
        {
            Console.WriteLine();
            int index;
            do { Console.Write(OutputData.NumberForEdit); }
            while (!Int32.TryParse(Console.ReadLine(), out index));
            var tmpclient = srv.GetClients()[index - 1];
            if (tmpclient == null)
                return;
            else
            {
                Console.WriteLine();
                Console.WriteLine(OutputData.EditDataClient + "\n");
                String[] newTour = new string[6];
                newTour[0] = InputDataForEdit(OutputData.AddClientName, tmpclient.Fio);
                newTour[1] = InputDataForEdit(OutputData.AddClientTel, tmpclient.Tel);
                newTour[2] = InputDataForEdit(OutputData.AddClientEmail, tmpclient.Email);
                newTour[3] = InputDataForEdit(OutputData.AddClientDiscount, tmpclient.Discount.ToString());

                srv.UpdateClient(new Client(tmpclient.Id, newTour[0], newTour[1], newTour[2], double.Parse(newTour[3].Replace(".", ","))));              
            }
        }

        private void RemoveClient()
        {
            Console.WriteLine();
            bool isNum = true;
            int numberForRemove = 0;
            int countClients = srv.GetClients().Count;
            do
            {
                Console.Write(OutputData.NumberForRemove);
                isNum = Int32.TryParse(Console.ReadLine(), out numberForRemove);
            }
            while (!(isNum && numberForRemove <= countClients));

            srv.RemoveClient(srv.GetClients()[numberForRemove - 1]);
        }
        
       private void ShowAllClients()
        {
            Console.Title = OutputData.MenuClient;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Clear();
            Console.WriteLine(OutputData.FooterClients);
            var clients = srv.GetClients();
            for (int i = 0; i < clients.Count; i++)
            {
                Console.WriteLine("{0}. {1}", i + 1, clients[i]);
            }
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(OutputData.MessageDelay);
            Console.ReadKey();
        }

    }
}
