﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;

namespace TourAgency
{
    static class AgencySettings
    {
        /// <summary>
        /// Получаем корневую папку для солюшина
        /// </summary>
        private static string dirPath = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;

        //public static string ClientFileName
        //{
        //    get
        //    {
        //        return GetFileName("clientStorage");
        //    }
        //}

        //public static string TourFileName
        //{
        //    get
        //    {
        //        return GetFileName("tourStorage");
        //    }
        //}

        //public static string JournalFileName
        //{
        //    get
        //    {
        //        return GetFileName("journalStorage");
        //    }
        //}

        //public static string clientsTourFileName
        //{
        //    get
        //    {
        //        return GetFileName("clientsTourStorage");
        //    }
        //}

        /// <summary>
        /// Получаем все названия файлов данных из конфигурационного файла проекта
        /// </summary>
        /// <returns>список файлов(полный путь) данных</returns>
        public static List<string> GetFileNames()
        {
            string[] appSettings = ConfigurationManager.AppSettings.AllKeys;
            List<string> files = new List<string>();


            for (int i = 0; i < appSettings.Length; i++)
            {
                files.Add(string.Format("{0}\\{1}", dirPath, ConfigurationManager.AppSettings[appSettings[i]]));
            }

            return files;
        }

    }
}
