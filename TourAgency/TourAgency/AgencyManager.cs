﻿using BusinessLogic;
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace TourAgency
{
    public delegate void CallMethod();

    partial class AgencyManager
    {
        Service srv;
        private Dictionary<char, CallMethod> dictMainMenu;
        private Dictionary<int, CallMethod> dictMenuAction;
        private Dictionary<int, CallMethod> dictMenuSortAndFilter; 
        private Dictionary<Entity, CallMethod> dictionaryShowEntity;

        #region Конструктор
        public AgencyManager()
        {
            #region Инициализация данных
            // возможно необходимо вынести в отдельный метод инициализации данных
            List<string> fileNames = AgencySettings.GetFileNames();
            srv = new Service(fileNames);
            #endregion

            srv.EventShowEntities += ShowEntities;

            InitializeDictionaries();

            SelectMode();

            srv.SaveData();
        } 
        #endregion

        private void InitializeDictionaries()
        {
            // словарь для  Event 
            dictionaryShowEntity = new Dictionary<Entity, CallMethod>()
            {
                {Entity.Client, ShowAllClients},
                {Entity.Tour, ShowAllTours},
                {Entity.Order, ShowAllOrders}
            };

            // словарь для главноего меню
            dictMainMenu = new Dictionary<char, CallMethod>()
            {
                { '0', SelectMode },
                { '1', WorkOfClients },
                { '2', WorkOfTours },
                { '3', WorkOfJournal }
            };
        }

        // вывод на экран листов после изменений 
        private void ShowEntities(object sender, AgencyEventArgs e)
        {
            dictionaryShowEntity[e.NameEntity]();
        }

        private void ExitMenu()
        {
            return;
        }

        public void SelectMode()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine();
            PrintTextOnCenter(OutputData.NameAgency);
            //byte mode;
            //do { Console.Write("\n" + OutputData.SelectMode);}
            //while(!byte.TryParse(Console.ReadLine(),out mode));

            Console.Write(OutputData.SelectMode);
            Console.ForegroundColor = ConsoleColor.White;

            ConsoleKeyInfo mode = new ConsoleKeyInfo();
            while (true)
            {
                if (Console.KeyAvailable == true)
                {
                    mode = Console.ReadKey(true);
                    if (mode.Key == ConsoleKey.Escape || mode.KeyChar == '0') break;  // если нажали Esc или 0 - выход
                    if (mode.Modifiers == ConsoleModifiers.Alt)   // если удерживается клавиша Alt
                    {
                        switch (mode.KeyChar)
                        {
                            case '0': break;
                            case '1': WorkAdmin(); break;        // и дальше нажали 1, то вызов меню для Админа
                            case '2': WorkUser(); break;         // если дальше нажали 2, то вызов меню для Пользователя
                        }
                    }
                }
            }
        }

        private void WorkAdmin()
        {
            int choice;
            //bool isNum = true;
            Console.Clear();
            Console.Title = "Admin";
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            PrintTextOnCenter(OutputData.NameAgency);
            Console.WriteLine("\n" + OutputData.SelectAction + "\n");
            Console.WriteLine(OutputData.MenuClient);
            Console.WriteLine(OutputData.MenuTour);
            Console.WriteLine(OutputData.MenuJournal);
            Console.WriteLine(OutputData.MenuExit);
            //do
            //{
            //    Console.Write("\n" + OutputData.Choise);
            //    isNum = Int32.TryParse(Console.ReadLine(), out choice);
            //}
            //while (!(isNum && choice <= 3 ));

            Console.Write(OutputData.Choise);
            ConsoleKeyInfo mode = new ConsoleKeyInfo();
            while (true)
            {
                if (Console.KeyAvailable == true)
                {
                    mode = Console.ReadKey(true);
                    if (mode.Key == ConsoleKey.Escape)   break;
                    if (dictMainMenu.ContainsKey(mode.KeyChar))   // проверка наличия ключа в словаре
                    {
                        Console.Write(mode.KeyChar);      // выводим то, что ввели
                        dictMainMenu[mode.KeyChar]();     // вызов соответствующего пункта меню, если таковой есть в словаре
                        break;
                    }
                }
            }
        }


        private int SelectAction(string title,string menuInfo)
        {
            int choice = 0;
            bool isNum = true;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            Console.Title = "Admin - " + title;
            PrintTextOnCenter(title.Substring(3, title.Length - 3));
            Console.WriteLine(OutputData.SelectAction);
            Console.WriteLine("\n" + OutputData.MenuAdd);
            Console.WriteLine(OutputData.MenuUpdate);
            Console.WriteLine(OutputData.MenuRemove);
            Console.WriteLine(OutputData.MenuView);
            Console.WriteLine(menuInfo);
            Console.WriteLine(OutputData.MenuSortAndFilter);
            //Console.WriteLine(OutputData.MenuSortOrdersById);
            //Console.WriteLine(OutputData.MenuSortOrdersByPrice);
            
            Console.WriteLine(OutputData.MenuExit);
            do
            {
                Console.Write(OutputData.Choise);
                isNum = Int32.TryParse(Console.ReadLine(), out choice);
            }
            while (!(isNum && dictMenuAction.ContainsKey(choice)));

            dictMenuAction[choice]();

            return choice;
        }

        private static void PrintTextOnCenter(string text)
        {
            int width = Console.WindowWidth;
            //if (text.Length > width)
            //    throw new ArgumentException("too long text", "text");
            string line = text.PadLeft((width + text.Length) >> 1).PadRight(width);
            Console.Write(line);
        }

        private string InputDataForEdit(string cap, string oldData)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n" + cap);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}{1}", OutputData.OldValue, oldData);
            Console.Write(OutputData.NewValue);
            string tmpField = Console.ReadLine();
            if (String.IsNullOrWhiteSpace(tmpField))
                tmpField = oldData;
            return tmpField;
        }

    }
}

